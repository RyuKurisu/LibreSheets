# LibreSheets

LibreSheets aims to become a cross platform, open source program to display your music sheets. 

LibreSheets will have at least :

* [ ]  Sheets management (list all songs, grouped per ensemble and set lists)
* [ ]  Auto-scrolling, Bluetooth pedal control and jump-links (for D.C./D.S. and Coda jumps etc)
* [ ]  Rehearsals at home
-   - [ ] Play audio files (midi, mp3, opus)
    - [ ] Scrub youtube links for audio
* [ ]   Support visual files:
-   - [ ]   .PDF
    - [ ]   (multipage) image files (jpeg,bmp,etc)
    - [ ]   .ly (lilypond engraving to PDF)
    - [ ]   .midi    

* [ ]  Non-destructive PDF editing¹ and annotation²
1.  - [ ]  cropping
    - [ ]  merging/splitting
    - [ ]  fine-grained rotation
2.  - [ ]  stamps 
    - [ ]  highlighs
    - [ ]  freehand

**Suggestions and contributions are very welcome!**
